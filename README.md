# ESP8266心知天气+时钟+WS2812点阵屏

#### 介绍
2020年春节完成的一个基于ESP8266的小项目，可以在0.96寸OLED和8*32RGB点阵上显示当前时间，时间来源有网络NTP时钟和本地DS3231两种，并且可以将网络时间更新到DS3231上。也写了简单的设置菜单功能。

#### 硬件连接



#### 开发环境

VS code下Platform IO插件 或  Arduino IDE

#### 使用方式

1.  如果你使用的是VScode开发环境，您直接下载后将此工程添加到你的工程目录即可。
2.  如果你使用的是Arduino IDE，您可以将 ESP8266_weather/lib/ 目录下的所有库拷贝到你的 文档\Arduino\libraries 目录下ESP8266_weather/include/ 目录下的weather.h 文件和 ESP8266_weather/src/ 目录下的main.cpp放在你新建的文件夹内，然后将main.cpp重命名为weather.ino。双击weather.ino打开->选择ESP8266开发板->上传程序，然后你就可以看到效果了。
3.  项目简介请查看我的博客：https://blog.csdn.net/qq_41868901/article/details/104225816


#### 码云特技
1.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
